export const MOCK_LAPTOPS = [

    {
        id: 1,
        name: "Huawei Matebook X 2020",
        price: 16990,
        description: "Huawei MateBook X 2020 bärbar dator har ett supertunt och elegant aluminiumchassi med en tjocklek på endast 1,36 cm, vilket gör den enkel att ta med. Den 13 tum stora pekskärmen rymmer nästan hela framsidan, för en sömlös tittarupplevelse.",
        imgLink: 'https://www.elgiganten.se/image/dv_web_D180001002530204/208084/huawei-matebook-x-2020-baerbar-dator-silver-frost.jpg?$fullsize$'
    },
  
    {
        id: 2,
        name: "Lenovo Ideapad 3 14",
        price: 3990,
        description: "Lenovo Ideapad 3 14-tums bärbar dator är ett utmärkt val för dig som är på resande fot. Datorn har en vikt på endast 1,2 kg och en supertunn profil på 1,79 cm, vilket gör den enkel att ta med sig överallt.",
        imgLink: 'https://www.elgiganten.se/image/dv_web_D180001002461329/176884/lenovo-ideapad-3-14-baerbar-dator-platinagraa.jpg?$fullsize$'
    },

    {
        id: 3,
        name: "Samsung Galaxy Book S 13",
        price: 14600,
        description: "Samsung Galaxy Book S bärbar dator kombinerar extremt lätt vikt med hög prestanda som klarar av mer krävande uppgifter. Tillsammans med dess LED-skärm kan du ta dig an arbetsuppgifter som kräver hög färgprecision.",
        imgLink: 'https://www.elgiganten.se/image/dv_web_D180001002521243/181552/samsung-galaxy-book-s-13-baerbar-dator-mercury-gray.jpg?$fullsize$'
    },

    {
        id: 4,
        name: "HP 14-dq0003 14",
        price: 2790,
        description: "HP 14-dq0003 bärbar dator har en smidig design med en lätt vikt på 1,5 kg och en effektiv processor som ger lång batteritid. Arbeta på resande fot eller koppla av framför din favoritserie med den 14 tum stora FHD-skärmen och dubbla högtalare",
        imgLink:'https://www.elgiganten.se/image/dv_web_D180001002464638/179356/hp-14-dq0003-14-baerbar-dator-vit.jpg?$fullsize$'
    },
    
    {
        id: 5,
        name: "MacBook Air 2020 13.3 256 GB",
        price: 10989,
        description: "MacBook Air 2020 ger dit tillförlitlig prestanda, snygg och lätt design som du enkelt kan ta med dig under dagen. Med en vikt på endast 1.3 kg och kraftfulla komponenter får du högre prestanda och bättre egenskaper jämfört med tidigare modeller",
        imgLink:'https://www.elgiganten.se/image/dv_web_D180001002461053/161645/macbook-air-2020-133-256-gb-rymdgraa.jpg?$fullsize$'
    },
    
    {
        id: 6,
        name: "MacBook Pro 16 2019 16/512 GB",
        price: 27489,
        description: "MacBook Pro 16 får jobbet gjort smidigt och effektivt med en klassledande 6-core Intel Core-processor och Radeon Pro-grafik. Den har ett precist Magic-tangentbord som är skönt att skriva på, uppgraderade högtalare och mikrofoner med studiokvalitet",
        imgLink:'https://www.elgiganten.se/image/dv_web_D180001002314629/81234/macbook-pro-16-2019-16512-gb-rymdgraa.jpg?$fullsize$'

    }
    

];