# Computer Store App in Javascript
### A vanilla Javascript app that displays a list of computers, where the user can navigate between them, and try to buy one of them. In order to buy a computer, the user need to have enough money in his/her balance, the user can work and bank the money to the balance. In addition, the user could also take a loan, however, the loan value must not exceed the double amount of the user's balance. Futhermore, the user can choose to either pay back the loan directly, by clicking the pay loan button which will pay the full amount of the work balance to the loan, or through the bank button which will only subtract 10% of the work money and transfer the rest to the user's balance.

## Resources
- Using bootstrap for some styling
- Laptop details are copied from elgiganten's webstore.

## Run
Open using any HTTP server

### ![screenshot](https://gitlab.com/iamamir/computerstore/-/raw/master/HomePage.PNG)
